import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';

class Answer extends StatelessWidget {
  final String answerText;
  final VoidCallback changeQuestion;
  const Answer(
      {super.key, required this.answerText, required this.changeQuestion});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      padding: const EdgeInsets.only(left: 20, right: 20),
      child: ElevatedButton(
        onPressed: changeQuestion,
        style: ElevatedButton.styleFrom(
          backgroundColor: Colors.amber,
        ),
        child: Text(answerText),
      ),
    );
  }
}
