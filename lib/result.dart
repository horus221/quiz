import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';

class Result extends StatelessWidget {
  final int score;
  final VoidCallback reset;
  const Result({super.key, required this.score, required this.reset});

  String get scoreText {
    if (score < 6) {
      return "You are innocent";
    } else if (score < 10) {
      return "YOu are likely good";
    }

    return "YOu are bad";
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          alignment: Alignment.center,
          margin: const EdgeInsets.only(top: 32),
          child: Text(
            scoreText,
            style: const TextStyle(
              fontSize: 24,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        OutlinedButton(
          onPressed: reset,
          style: OutlinedButton.styleFrom(
            foregroundColor: Colors.amber,
          ),
          child: const Text("Retry"),
        )
      ],
    );
  }
}
