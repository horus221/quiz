import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:quiz/quiz.dart';
import 'package:quiz/result.dart';

void main() {
  runApp(const Main());
}

class Main extends StatefulWidget {
  const Main({super.key});

  @override
  State<Main> createState() => _MainState();
}

class _MainState extends State<Main> {
  int _questionIndex = 0;
  int _totalScore = 0;
  final List<Map<String, Object>> _questions = [
    {
      "questionText": "What is your favorite colour?",
      'answers': [
        {'answerText': 'White', 'score': 1},
        {'answerText': 'Green', 'score': 3},
        {'answerText': 'Brown', 'score': 5},
        {'answerText': 'Black', 'score': 7},
      ]
    },
    {
      "questionText": "What is your favorite animal?",
      'answers': [
        {'answerText': 'Rabbit', 'score': 1},
        {'answerText': 'Cat', 'score': 3},
        {'answerText': 'Monkey', 'score': 5},
        {'answerText': 'Snake', 'score': 7},
      ]
    },
    {
      "questionText": "What is your favorite music type?",
      'answers': [
        {'answerText': 'Symphony', 'score': 1},
        {'answerText': 'Pop', 'score': 3},
        {'answerText': 'Hip Hop', 'score': 5},
        {'answerText': 'Rocck/Metal', 'score': 7},
      ]
    },
  ];

  _changeQuestion(int score) {
    _totalScore += score;
    setState(() {
      _questionIndex++;
    });
  }

  _reset() {
    setState(() {
      _totalScore = 0;
      _questionIndex = 0;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text("Quiz app"),
          backgroundColor: Colors.amber,
        ),
        body: Column(
          children: [
            _questions.length > _questionIndex
                ? Quiz(
                    questions: _questions,
                    questionIndex: _questionIndex,
                    changeQuestion: _changeQuestion,
                  )
                : Result(
                    score: _totalScore,
                    reset: _reset,
                  )
          ],
        ),
      ),
    );
  }
}
