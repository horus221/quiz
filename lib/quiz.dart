import 'package:flutter/cupertino.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:quiz/answer.dart';
import 'package:quiz/question.dart';

class Quiz extends StatelessWidget {
  final List<Map<String, Object>> questions;
  final int questionIndex;
  final Function changeQuestion;

  const Quiz(
      {super.key,
      required this.questions,
      required this.questionIndex,
      required this.changeQuestion});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Question(question: questions[questionIndex]['questionText'] as String),
        ...(questions[questionIndex]['answers'] as List<Map<String, Object>>)
            .map(
          (e) => Answer(
            answerText: e['answerText'] as String,
            changeQuestion: () => changeQuestion(e['score'] as int),
          ),
        )
      ],
    );
  }
}
