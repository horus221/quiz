# quiz

## Name

Quiz

## Description

Quiz is an app that test the personality of the user based on the answers they give on the particular questions.

This is the first app that I am building with flutter. It comes from a tutorial on udemy named [Flutter & Dart - The Complete Guide [2022 Edition]] by [Maximillian Shawrmüller]

## Authors and acknowledgment

Babacar Kane
Maximillian Shawrmüller
